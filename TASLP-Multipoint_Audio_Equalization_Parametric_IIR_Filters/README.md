Companion files for the paper "Deep Optimization of Parametric IIR Filters for Audio Equalization" presented in IEEE/ACM Transactions on Audio, Speech, and Language Processing.
The Audio files are recorded from the simulations. The input file is a mono file sampled to 48 kHz. The recorded files are sampled at 48 kHz.
The audio files have been named according to the algorithm used, the scenario and the optimization scheme (SISO, MISO or MIMO).
With the term 'SISO' the reproduction occured using one speaker, whereas with the term "MISO" and "MIMO" all available speakers have been used. 
Three techniques are used: the Frequency Deconvolution (FD), the Direct Search Method (DSM) and the neural proposed method (BiasNet). 'FD_1024' stands for FIR filters of 1024-th order designed with FD. 'FD_' stands for FIR filters of 8192-th order designed with FD.
List of each sub-directory:
- Reference.wav: Mono audio file used for the reproduction
- Unfiltered_room_SISO.wav
- Unfiltered_room_MISO.wav
- Unfiltered_room_MIMO.wav
- Unfiltered_car_MIMO.wav
- FD_room_SISO.wav
- FD_room_MISO.wav
- FD_room_MIMO.wav
- FD_1024_room_SISO.wav
- FD_1024_room_MISO.wav
- FD_1024_room_MIMO.wav
- FD_car_MIMO.wav
- DSM_room_SISO.wav
- DSM_room_MISO.wav
- DSM_room_MIMO.wav
- DSM_car_MIMO.wav
- BiasNet_room_SISO.wav
- BiasNet_room_MISO.wav
- BiasNet_room_MIMO.wav
- BiasNet_car_MIMO.wav
