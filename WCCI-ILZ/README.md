Companion files for the paper "Digital Filters Design for Personal Sound Zones: a Neural Approach" to be presented at the 2022 WCCI, Padua.

List of files in this folder: 2 sub-directories composed of audio files.
The Audio files are recorded from the simulations. The input files have been extracted from LibriSpeech dataset and then oversampled to 48 kHz. The recorded files are sampled at 48 kHz.
The audio files have been named according to the algorithm used, the position of the bright and dark zones and the pair of microphones used.
Three techniques are used: the Acoustic Contrast Control (ACC), the Pressure Matching (PM) and the neural proposed method (BiasNet).

Each file, except the reference and the unfiltered audio files, are denominated as:

"method"_BZ_"location bright zone"_DZ_"location dark zone"_mic_"pair of microphones location".wav

The other files are denominated as:
    - Reference.wav: Mono female voice used for the reproduction
    - Unfiltered_mic_DS: recorder audio files from driver position with no filters
    - Unfiltered_mic_PS: recorder audio files from passenger position with no filters


List of each sub-directory:

    - ACC_BZ_DS_DZ_PS_mic_DS.wav
    - ACC_BZ_DS_DZ_PS_mic_PS.wav
    - ACC_BZ_PS_DZ_DS_mic_DS.wav
    - ACC_BZ_PS_DZ_DS_mic_PS.wav
    - PM_BZ_DS_DZ_PS_mic_DS.wav
    - PM_BZ_DS_DZ_PS_mic_PS.wav
    - PM_BZ_PS_DZ_DS_mic_DS.wav
    - PM_BZ_PS_DZ_DS_mic_PS.wav
    - BiasNet_BZ_DS_DZ_PS_mic_DS.wav
    - BiasNet_BZ_DS_DZ_PS_mic_PS.wav
    - BiasNet_BZ_PS_DZ_DS_mic_DS.wav
    - BiasNet_BZ_PS_DZ_DS_mic_PS.wav
    - Unfiltered_mic_DS.wav
    - Unfiltered_mic_PS.wav
    - Reference.wav


