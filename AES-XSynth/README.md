Companion files for the paper "Deep Learning for Timbre Modification and Transfer: an Evaluation Study" to be presented at the 144th AES Convention, Milan, 2018.

List of files in this folder:
- FV1.wav - female voice referred to in the paper
- SweetChildGuitar.wav - Guitar only excerpt from Sweet Child of Mine by Guns'n'Roses
- ProposedApproach.wav - Output of the proposed neural network architecture trained with the guitar track, using FV1 as input
- MFCCMatch.wav - Output of the MFCC matching algorithm using the guitar track as dictionary and FV1 as input
- SpectralFlatten.wav - Output of the spectral envelope hybridization using the guitar track and FV1 as input.
